import request from '@/utils/request'

// 查询数据源列表
export function listSource(query) {
  return request({
    url: '/code/source/list',
    method: 'get',
    params: query
  })
}// 查询数据源列表
export function getAll(query) {
  return request({
    url: '/code/source/getAll',
    method: 'get',
    params: query
  })
}

// 查询数据源详细
export function getSource(id) {
  return request({
    url: '/code/source/' + id,
    method: 'get'
  })
}

// 新增数据源
export function addSource(data) {
  return request({
    url: '/code/source',
    method: 'post',
    data: data
  })
}

// 修改数据源
export function updateSource(data) {
  return request({
    url: '/code/source',
    method: 'put',
    data: data
  })
}

// 删除数据源
export function delSource(id) {
  return request({
    url: '/code/source/' + id,
    method: 'delete'
  })
}
