package com.ruoyi.gen.controller;

import com.common.zrd.json.CommonJsonResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ruoyi.gen.domain.GenTemplate;
import com.ruoyi.gen.domain.entity.GenTemplateEntity;
import com.ruoyi.gen.service.IGenTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 代码生成模板管理1Controller
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@RestController
@RequestMapping("/template" )
public class GenTemplateController extends BaseController {
    @Autowired
    private IGenTemplateService genTemplateService;

    /**
     * 查询代码生成模板管理1列表
     */
    @PreAuthorize(hasPermi = "gen:template:list" )
    @GetMapping("/list" )
    public TableDataInfo list(GenTemplateEntity genTemplate) {
        startPage();
        List<GenTemplateEntity> list = genTemplateService.selectGenTemplateList(genTemplate);
        return getDataTable(list, GenTemplateEntity.class);
    }

    /**
     * 导出代码生成模板管理1列表
     */
    @PreAuthorize(hasPermi = "gen:template:export" )
    @Log(title = "代码生成模板管理1" , businessType = BusinessType.EXPORT)
    @PostMapping("/export" )
    public void export(HttpServletResponse response, GenTemplateEntity genTemplate) throws IOException {
        List<GenTemplateEntity> list = genTemplateService.selectGenTemplateList(genTemplate);
        ExcelUtil<GenTemplateEntity> util = new ExcelUtil<>(GenTemplateEntity.class);
        util.exportExcel(response, list, "template" );
    }

    /**
     * 获取代码生成模板管理1详细信息
     */
    @PreAuthorize(hasPermi = "gen:template:query" )
    @GetMapping(value = "/{id}" )
    public AjaxResult getInfo(@PathVariable("id" ) Long id) {
        return AjaxResult.success(genTemplateService.getById(id));
    }

    /**
     * 新增代码生成模板管理1
     */
    @PreAuthorize(hasPermi = "gen:template:add" )
    @Log(title = "代码生成模板管理1" , businessType = BusinessType.INSERT)
    @PostMapping
    public CommonJsonResult add(@RequestBody GenTemplate genTemplate) {
        genTemplateService.save(genTemplate);
        return CommonJsonResult.of(genTemplate);
    }

    /**
     * 修改代码生成模板管理1
     */
    @PreAuthorize(hasPermi = "gen:template:edit" )
    @Log(title = "代码生成模板管理1" , businessType = BusinessType.UPDATE)
    @PutMapping
    public CommonJsonResult edit(@RequestBody GenTemplate genTemplate) {
        genTemplateService.updateById(genTemplate);
        return CommonJsonResult.of(genTemplate);
    }

    /**
     * 删除代码生成模板管理1
     */
    @PreAuthorize(hasPermi = "gen:template:remove" )
    @Log(title = "代码生成模板管理1" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}" )
    public CommonJsonResult remove(@PathVariable Long[] ids) {

        genTemplateService.removeByIds(Arrays.asList(ids));
        return CommonJsonResult.empty();
    }
}
