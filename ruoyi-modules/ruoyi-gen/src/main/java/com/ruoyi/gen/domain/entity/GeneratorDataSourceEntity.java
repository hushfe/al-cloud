package com.ruoyi.gen.domain.entity;

import com.ruoyi.gen.domain.GeneratorDataSource;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 数据源对象 generator_data_source
 *
 * @author ruoyi
 * @date 2021-04-12
 */

@ApiModel("数据源")
@Data

public class GeneratorDataSourceEntity extends GeneratorDataSource {


    private static final long serialVersionUID = 7321203873733968130L;
}
