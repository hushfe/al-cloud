package com.ruoyi.common.core.factor;

import com.ruoyi.common.core.api.RemoteTranNewService;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.SysDictDataVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 日志服务降级处理
 *
 * @author ruoyi
 */
@Component
@Slf4j
public class RemoteTranNewFallbackFactory implements FallbackFactory<RemoteTranNewService> {


    @Override
    public RemoteTranNewService create(Throwable throwable) {
        RemoteTranNewFallbackFactory.log.error("翻译服务调用失败:{}", throwable.getMessage());
        return new RemoteTranNewService() {

            @Override
            public R<List<SysDictDataVo>> getAllDictData(String dictType) {

                return R.fail();
            }
        };
    }
}
